const totalFuelNeeded = require('../totalFuelNeeded')

test('Test 1: 14 mass should return 2 fuel', () => {
  expect(totalFuelNeeded([14])).toBe(2)
})
test('Test 2: 1969 mass should return 966 fuel', () => {
  expect(totalFuelNeeded([1969])).toBe(966)
})
test('Test 3: 100756 should return 50346 fuel', () => {
  expect(totalFuelNeeded([100756])).toBe(50346)
})
