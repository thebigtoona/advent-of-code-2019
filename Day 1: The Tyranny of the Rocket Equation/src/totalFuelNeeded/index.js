const fs = require('fs')
const inputFile = `${__dirname}/input.txt`

const fuelEquation = x => Math.floor(x / 3) - 2
const calculateRocketFuel = mass => fuelEquation(mass)

const calcAdditionalFuel = (mass, computedFuels = []) =>
  calculateRocketFuel(mass) > 0
    ? calcAdditionalFuel(calculateRocketFuel(mass), [
        calculateRocketFuel(mass),
        ...computedFuels
      ])
    : computedFuels.reduce((totalFuel, fuel) => totalFuel + fuel)

// get inputs
const inputs = fs
  .readFileSync(inputFile)
  .toString()
  .split('\n')
  .map(string => Number(string))

const totalFuelNeeded = inputs =>
  inputs
    .map(mass => calcAdditionalFuel(mass))
    .reduce((totalFuel, fuel) => totalFuel + fuel)

console.log(`Total Fuel Needed: ${totalFuelNeeded(inputs)}`)

module.exports = totalFuelNeeded
