const fs = require('fs')
const inputFile = `${__dirname}/input.txt`
const file = fs.existsSync(inputFile)
  ? fs
      .readFileSync(inputFile)
      .toString()
      .split(',')
      .map(n => Number(n))
  : null

const initState = (file) => file

const replace = (value, pos, array) =>
  pos <= array.length
    ? (array = [
        ...array.slice(0, pos),
        value,
        ...array.slice(pos + 1, array.length)
      ])
    : false

const opcode = (state, address = 0, inc = 0, range = 4, map = new Map()) =>
  inc < range && map.size < range
    ? map.set(inc, state[address]) &&
      opcode(state, address + 1, inc + 1, range, map)
    : map

const instruction = (state, address) => opcode(state, address).get(0) || false
const parameter = (opcode, address) => opcode.get(address)

const add = (opcode, state) =>
  replace(
    state[parameter(opcode, 1)] + state[parameter(opcode, 2)],
    parameter(opcode, 3),
    state
  )

const multiply = (opcode, state) =>
  replace(
    state[parameter(opcode, 1)] * state[parameter(opcode, 2)],
    parameter(opcode, 3),
    state
  )

const generateExitCode = (noun, verb) => 100 * noun + verb

const gravityAssist = (state = initState(file), pointer = 0, range = 4) =>
  state.length < range
    ? state
    : instruction(state, pointer) === 1
    ? gravityAssist(add(opcode(state, pointer), state), pointer + 4)
    : instruction(state, pointer) === 2
    ? gravityAssist(multiply(opcode(state, pointer), state), pointer + 4)
    : instruction(state, pointer) === 99
    ? state.toString()
    : false

// start part 2
const changedState = (noun, verb, state = initState(file)) => [
  state[0],
  noun,
  verb,
  ...state.slice(3, state.length)
]

const newOutput = (noun, verb) => gravityAssist(changedState(noun, verb))

const findTargetOutput = (noun = 0, verb = 0, target = 19690720) =>
  Number(newOutput(noun, verb).split(',')[0]) === target
    ? newOutput(noun, verb)
    : verb !== 99
    ? findTargetOutput(noun, verb + 1)
    : verb === 99 && noun < 99
    ? findTargetOutput(noun + 1, 0)
    : `no valid answer`

const alarmCode = array => generateExitCode(Number(array[0]), Number(array[1]))

console.log({
  part1Answer: gravityAssist(changedState(12, 2)).split(',')[0],
  targetProgram: findTargetOutput(),
  targetAlarmCode: alarmCode([
    ...findTargetOutput()
      .split(',')
      .slice(1, 3)
  ])
})

module.exports = gravityAssist
