const gravityAssistProgram = require('../src/programAlarm')

test('Day 2: Gravity Assist Program Test 1', () => {
  expect(gravityAssistProgram([1,0,0,0,99])).toBe([2,0,0,0,99].toString());
})


test('Day 2: Gravity Assist Program Test 2', () => {
  expect(gravityAssistProgram([2,3,0,3,99])).toBe([2,3,0,6,99].toString());
})


test('Day 2: Gravity Assist Program Test 3', () => {
  expect(gravityAssistProgram([1, 1, 1, 4, 99, 5, 6, 0, 99])).toBe(
    [30, 1, 1, 4, 2, 5, 6, 0, 99].toString()
  );
})